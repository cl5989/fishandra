import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from .new_constants import constants as cons
from .maps_utils import (produce_grid_figures, produce_subnetwork_output,
                         produce_mesh, remove_strange_white_lines,
                         give_me_cbars)


def generate_cmap(c2):
    c_0 = mpl.colors.colorConverter.to_rgba('white', alpha=0.0)
    c_2 = mpl.colors.colorConverter.to_rgba(c2)
    return mpl.colors.LinearSegmentedColormap.from_list(
        'rb_cmap', [c_0, c_2], 512)


def generate_cmap2(c0, c2):
    c_0 = mpl.colors.colorConverter.to_rgba(c0)
    c_1 = mpl.colors.colorConverter.to_rgba('white', alpha=0.0)
    c_2 = mpl.colors.colorConverter.to_rgba(c2)
    return mpl.colors.LinearSegmentedColormap.from_list(
        'rb_cmap', [c_0, c_1, c_2], 512)


def cmap_alignment():
    return generate_cmap2('tab:pink', 'black')


def cmap_attraction():
    return plt.cm.PuOr_r


def generate_angles_to_average(num_angles):
    return np.linspace(0, 2 * np.pi, num_angles, endpoint=False)


def compute_Z_multiple_angles(model, X, Y, vf=None, vn=None, af=0, an=0):
    angles = generate_angles_to_average(cons.NUM_ANGLES_AVERAGE)
    Z_angles = [produce_subnetwork_output(model, 'pairwise_model', X, Y,
                                          vf=vf, vn=vn, an=an, angle_vn=angle)
                for angle in angles]

    return np.asarray(Z_angles)


def compute_scores(Z_angles):
    """compute_scores

    :param Z_angles: A list of bidimensional arrays
        representig logit maps in different angles
    """
    # Z_angles = np.asarray(Z_angles)
    angles = np.asarray(generate_angles_to_average(Z_angles.shape[0]))
    angles_sign = np.sign(np.sin(angles))[:, np.newaxis, np.newaxis]

    # Attraction-repulsion score
    Z_average = np.mean(Z_angles, axis=0)
    # Alignment-antialignment
    Zmax_with_angle = np.max(Z_angles * angles_sign, axis=0).clip(0)
    Zmax_against_angle = np.max(-Z_angles * angles_sign, axis=0).clip(0)
    # Frontiers between atraction-repulsion and alignment-antialignment
    Zmin, Zmax = np.min(Z_angles, axis=0), np.max(Z_angles, axis=0)
    return Z_average, Zmax_with_angle-Zmax_against_angle, (Zmax*Zmin < 0)


class ProbPlot:
    def __init__(self, X, Y, Z, ax=None, cax=None,
                 limits=None, cmap=plt.cm.seismic,
                 cbar_label='logit turning right'):
        """Produces a probability/allignment plot

        :param X: bidimensional array of x-coordinates
        :param Y: bidimensional array of y-coordinates
        :param Z: bidimensional array of logits
        :param ax: axis to plot into
        :param cax: colobar, if any
        :param limits: limits of the colorbar
        :param cmap: color map
        :param cbar_label: label to add to the colorbar
        """

        self.X, self.Y, self.Z = X, Y, Z
        self.cmap = cmap

        if limits is None:
            print("Warning, default limits used")
            self.vmax = abs(Z).max()
            self.vmin = -self.vmax
        else:
            self.vmin, self.vmax = limits

        self.CS = self.contours(ax, cax, cbar_label)

        ax.add_patch(mpl.patches.Ellipse([0, 0], 0.3,
                                         1, fc='g', ec='k'))

    def add_axes_labels(self, ax):
        ax.set_xlabel(r"$x_i$ (BL)")
        ax.set_ylabel(r"$y_i$ (BL)")

    def contours(self, ax, cax, cbar_label):
        CS = ax.contourf(self.X, self.Y, self.Z,
                         levels=np.linspace(self.vmin, self.vmax, 20),
                         cmap=self.cmap, origin='lower')

        tick_locator = mpl.ticker.MaxNLocator(nbins=5)
        remove_strange_white_lines(CS)
        if cax is not None:
            cbar = cax.get_figure().colorbar(CS, cax=cax)
            cbar.locator = tick_locator
            cbar.set_label(
                cbar_label,
                rotation=90,
                labelpad=-1)
            cbar.update_ticks()

        return CS


class AttractionPlot(ProbPlot):
    def __init__(self, X, Y, Z, ax=None, cax=None,
                 limits=None, cbar_label='attraction-repulsion score',
                 cmap=cmap_attraction()):
        Z_sign = Z.copy()
        Z_sign[np.where(X < 0)] *= -1
        super().__init__(X, Y, Z_sign, ax=ax, cax=cax,
                         limits=limits, cmap=cmap, cbar_label=cbar_label)


class AllignmentPlot(ProbPlot):
    def __init__(self, X, Y, Z, ax=None, cax=None,
                 limits=None, cmap=cmap_alignment(),
                 cbar_label='alignment score'):
        super().__init__(X, Y, Z, ax=ax, cax=cax,
                         limits=limits, cmap=cmap, cbar_label=cbar_label)


class AreaPlot:
    def __init__(self, X, Y, meanZ, DZ, signchange,
                 ax=None, cax=None,
                 limits=[None, None],
                 cmap1=cmap_attraction()):

        # Z1 is like the mean Z when there is no change in sign in Z
        self.ax = ax
        Z1 = np.zeros_like(meanZ)
        Z1[np.where(~signchange)] = meanZ[np.where(~signchange)]

        # Z2 is like the Z alignment score when there is a change in sign in Z
        Z2 = np.zeros_like(meanZ)
        Z2[np.where(signchange)] = DZ[np.where(signchange)]

        self.attraction = AttractionPlot(X, Y, Z1, ax=ax, cax=cax[0],
                                         limits=limits[0])

        self.allignment = AllignmentPlot(X, Y, Z2, ax=ax, cax=cax[1],
                                         limits=limits[1])

    def add_axes_labels(self, ax):
        ax.set_xlabel(r"$x_i$ (BL)")
        ax.set_ylabel(r"$y_i$ (BL)")


def generate_titles():
    titles = [r"$v_i$ = {} BL/s".format(s) for s in cons.SPEED_LABEL_BL] +\
                [r"$v$ = {} BL/s".format(s) for s in cons.SPEED_LABEL_BL]
    return titles


def plot_fig_1(fig, X, Y, Zall, limits=None):
    maximum_valueZ = max([abs(Zi).max() for Zi in Zall])
    limits = [-maximum_valueZ, maximum_valueZ] if limits is None else limits

    ax_flat = produce_grid_figures(fig)
    cax = give_me_cbars(ax_flat, 1)

    plots = [
        AttractionPlot(X, Y, Zi.copy(),
            ax=axi, cax=cax, limits=limits, cmap=cmap_attraction())
        for axi, Zi in zip(ax_flat, Zall)]

    titles = generate_titles()
    for axi, title, plot in zip(ax_flat, titles, plots):
        axi.set_title(title)
        axi.xaxis.labelpad = -2
        axi.yaxis.labelpad = -2
        plot.add_axes_labels(axi)

    return limits


def plot_fig_2(fig, X, Y, Zall, limits=None):
    maximum_valueZ = max([abs(Zi).max() for Zi in Zall])
    limits = [-maximum_valueZ, maximum_valueZ] if limits is None else limits

    ax2_flat = produce_grid_figures(fig)
    cax2 = give_me_cbars(ax2_flat, 1)

    plots2 = [
        AllignmentPlot(X, Y, Zi.copy(),
                       ax=axi, cax=cax2, limits=limits)
        for axi, Zi in zip(ax2_flat, Zall)]

    titles = generate_titles()
    for axi, title, plot in zip(ax2_flat, titles, plots2):
        axi.set_title(title)
        plot.add_axes_labels(axi)

    return limits


def plot_fig_3(fig, X, Y, Zall, DZall, signchange, limits=None):
    ax3_flat = produce_grid_figures(fig)
    cax3_1, cax3_2 = give_me_cbars(ax3_flat, 2)

    plots3 = [AreaPlot(X, Y, Zi, DZi, sign_change,
                       ax=axi, cax=[cax3_1, cax3_2], limits=limits)
              for axi, Zi, DZi, sign_change in zip(ax3_flat, Zall,
                                                   DZall, signchange)]

    titles = generate_titles()
    for axi, title, plot in zip(ax3_flat, titles, plots3):
        axi.set_title(title)
        axi.xaxis.labelpad = -2
        axi.yaxis.labelpad = -2
        plot.add_axes_labels(axi)


def interactions_nb_focal_speed_figure_raw_data(model):
    print("Num neighbours: ", model.args["num_neighbours"])
    print("Future steps: ", model.args["future_steps"])
    X, Y = produce_mesh()

    nb_speed = [compute_Z_multiple_angles(model, X, Y, vn=speed)
                for speed in cons.SPEED]
    focal_speed = [compute_Z_multiple_angles(model, X, Y, vf=speed)
                   for speed in cons.SPEED]

    return nb_speed + focal_speed, X, Y


def interactions_nb_focal_speed_figure_data(model):
    Z_raw, X, Y = interactions_nb_focal_speed_figure_raw_data(model)
    Z = [compute_scores(z) for z in Z_raw]
    return Z, X, Y


def interactions_single_figure_raw_data(model, **kwargs):
    print("Num neighbours: ", model.args["num_neighbours"])
    print("Future steps: ", model.args["future_steps"])
    X, Y = produce_mesh()
    return compute_Z_multiple_angles(model, X, Y, **kwargs), X, Y


def interactions_single_figure_data(model, **kwargs):
    Z, X, Y = interactions_single_figure_raw_data(model, **kwargs)
    return compute_scores(Z), X, Y


import numpy as np
from mpl_toolkits.axes_grid1 import ImageGrid

from .new_constants import constants as cons
from .maps_utils import (produce_subnetwork_output, produce_mesh)
from .interaction import (AreaPlot, compute_Z_multiple_angles, compute_scores)
from .attention import (attention_nb_focal_speed_figure_data,
                        attention_single_figure_data)


# angle for attention
ANGLE = 0  # Results should be independent of this

def produce_product_raw_maps(model, X, Y, attention, vf=None, vn=None,
                             af=0, an=0, k=None):
    Z_angles = compute_Z_multiple_angles(model, X, Y,
                                                 vf=vf, vn=vn, af=af, an=an)
    if k is None:
        multiplicative_factor = np.exp(attention)
    else:
        multiplicative_factor = np.exp(attention)/(np.exp(attention) + k)
    Z_product = [Z*multiplicative_factor for Z in Z_angles]
    return np.asarray(Z_product)


def produce_product_maps(model, X, Y, attention, vf=None, vn=None,
                         af=0, an=0, k=None):
    Z_product = produce_product_raw_maps(model, X, Y, attention,
                                         vf=vf, vn=vn, af=0, an=0,
                                         k=None)
    Z_mean, Z_alignment, Z_orientation_zone = compute_scores(Z_product)
    return Z_mean, Z_alignment, Z_orientation_zone


def offset_attention_in_place(attention_list):
    means = np.array([np.mean(pair[0]) for pair in attention_list])
    offset = np.mean(means)
    for pair in attention_list:
        pair[0] -= offset


def product_nb_focal_speed_figure_data(model, k=None):
    print("Num neighbours: ", model.args["num_neighbours"])
    print("Future steps: ", model.args["future_steps"])
    X, Y = produce_mesh()
    # Attention changing neighbour speed offsets with global mean
    attention_nb_speed = [[produce_subnetwork_output(
                               model, 'attention_model', X, Y, vn=speed,
                               angle_vn=ANGLE),
                           speed] for speed in cons.SPEED]
    offset_attention_in_place(attention_nb_speed)

    # Attention changing focal speed offsets with local mean
    attention_focal_speed = [[produce_subnetwork_output(
                                  model, 'attention_model', X, Y, vf=speed,
                                  angle_vn=ANGLE),
                              speed] for speed in cons.SPEED]
    for pair in attention_focal_speed:
        offset_attention_in_place([pair])

    nb_speed = [produce_product_maps(model, X, Y, attention, vn=speed, k=k)
                for attention, speed in attention_nb_speed]
    focal_speed = [produce_product_maps(model, X, Y, attention, vf=speed, k=k)
                   for attention, speed in attention_focal_speed]
    Z_values_models = nb_speed + focal_speed

    return Z_values_models, X, Y

def product_nb_focal_speed_figure_raw_data(model, k=None):
    print("Num neighbours: ", model.args["num_neighbours"])
    print("Future steps: ", model.args["future_steps"])
    Z_attention, X, Y = attention_nb_focal_speed_figure_data(model)
    attention_nb_speed = Z_attention[:4]
    attention_focal_speed = Z_attention[4:]
    nb_speed = [produce_product_raw_maps(model, X, Y, attention, vn=speed, k=k)
                for attention, speed in zip(attention_nb_speed, cons.SPEED)]
    focal_speed = [produce_product_raw_maps(model, X, Y, attention, vf=speed, k=k)
                   for attention, speed in zip(attention_focal_speed, cons.SPEED)]
    Z_values_models = nb_speed + focal_speed

    return Z_values_models, X, Y

def product_nb_focal_speed_figure_data(model, k=None):
    Z_raw_values_models, X, Y = product_nb_focal_speed_figure_raw_data(model, k=k)
    Z_values_models = [compute_scores(z) for z in Z_raw_values_models]
    return Z_values_models, X, Y



def product_single_figure_raw_data(model, **kwargs):
    print("Num neighbours: ", model.args["num_neighbours"])
    print("Future steps: ", model.args["future_steps"])
    attention, X, Y = attention_single_figure_data(model)
    return produce_product_raw_maps(model, X, Y, attention, **kwargs), X, Y

def product_single_figure_data(model, **kwargs):
    Z, X, Y = product_single_figure_raw_data(model, **kwargs)
    return compute_scores(Z), X, Y


# k-figure

K_RANGE = [0.1, 0.3, 1.0, 3.0, 10.0]

def product_k_figure_raw_data(model, **kwargs):
    print("Num neighbours: ", model.args["num_neighbours"])
    print("Future steps: ", model.args["future_steps"])
    attention, X, Y = attention_single_figure_data(model)

    Z_values_models = [produce_product_raw_maps(model, X, Y,
                                                attention, k=k, **kwargs)
                       for k in K_RANGE]
    return Z_values_models, X, Y


def product_k_figure_data(model, **kwargs):
    Z_raw_values_models, X, Y = product_k_figure_raw_data(model, **kwargs)
    Z_values_models = [compute_scores(z) for z in Z_raw_values_models]
    return Z_values_models, X, Y


def plot_fig_k(fig, X, Y, Zall, DZall, signchange, limits=None):
    assert len(Zall) == len(DZall) == len(signchange)
    ncols = len(Zall)
    grid = ImageGrid(fig, 111, axes_pad=(0.1, 0.1), nrows_ncols=(1, ncols))
    fig.subplots_adjust(right=0.8)
    bbox_axup = grid[0].get_position()
    bbox_axdown = grid[-1].get_position()

    cax_1 = fig.add_axes(
        [0.82, bbox_axdown.y0, 0.01, bbox_axup.y1-bbox_axdown.y0])
    cax_2 = fig.add_axes(
        [0.91, bbox_axdown.y0, 0.01, bbox_axup.y1-bbox_axdown.y0])

    plots = [AreaPlot(X, Y, Zi, DZi, sign_change,
                      ax=axi, cax=[cax_1, cax_2], limits=limits)
              for axi, Zi, DZi, sign_change in zip(grid, Zall,
                                                   DZall, signchange)]
    titles = [r'k = {}'.format(k) for k in K_RANGE]
    for axi, title, plot in zip(grid, titles, plots):
        axi.set_title(title)
        #axi.xaxis.labelpad = -2
        #axi.yaxis.labelpad = -2
        plot.add_axes_labels(axi)

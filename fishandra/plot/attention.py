import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


from .new_constants import constants as cons
from .maps_utils import (give_me_cbars, produce_grid_figures,
                         produce_subnetwork_output,
                         produce_mesh, remove_strange_white_lines)


ANGLE = np.pi
FIXED_MIN = None
FIXED_MAX = None


class AttentionPlot:
    def __init__(self, X, Y, Z, ax=None, limits=None):
        if limits is None:
            vmax = Z.max()
            vmin = Z.min()
        else:
            vmin, vmax = limits

        self.CS = ax.contourf(X, Y, Z,
                              cmap=plt.cm.YlGn,
                              levels=np.linspace(vmin, vmax, 20),
                              origin='lower')
        remove_strange_white_lines(self.CS)
        ax.contour(X, Y, Z, np.linspace(vmin, vmax, 7), colors='black', linewidths=0.1)
        ax.add_patch(mpl.patches.Ellipse([0,0], 0.3,
                                         1, fc='g', ec='k'))


def plot_fig(fig, X, Y, Zall, limits=None, nbvariation=None):
    ax_flat = produce_grid_figures(fig)

    if nbvariation is None or nbvariation == 'vel':
        titles = [r"$v_i$ = {} $BL/s$".format(s) for s in cons.SPEED_LABEL_BL] +\
                 [r"$v$ = {} $BL/s$".format(s) for s in cons.SPEED_LABEL_BL]
    elif nbvariation == 'acc':
        raise Exception('Not implemented')

    for a, title in zip(ax_flat, titles):
        a.set_aspect('equal')
        a.set_xlabel(r"$x_i$ (BL)")
        a.set_ylabel(r"$y_i$ (BL)")
        a.xaxis.labelpad = -2
        a.yaxis.labelpad = -2

        a.set_title(title)#, fontsize=8)

    if FIXED_MAX is None:
        maximum_value = max([Zi.max() for Zi in Zall])
    else:
        maximum_value = FIXED_MAX

    if FIXED_MIN is None:
        minimum_value = min([Zi.min() for Zi in Zall])
    else:
        minimum_value = FIXED_MIN

    if limits is None:
        limits = [minimum_value, maximum_value]

    plots = [AttentionPlot(X, Y, Zi,
                           ax=axi, limits=limits)
             for axi, Zi in zip(ax_flat, Zall)]

    cax = give_me_cbars(ax_flat)
    cbar = fig.colorbar(plots[-1].CS, cax=cax)
    cbar.locator = mpl.ticker.MaxNLocator(nbins=5, integer=True)
    cbar.set_label('logit attention', rotation=90, labelpad=-1)
    cbar.update_ticks()

def offset_attention_in_place(attention_list):
    means = np.array([np.mean(element) for element in attention_list])
    offset = np.mean(means)
    attention_list[:] = [element - offset for element in attention_list]

def offset_single_atention(attention_element):
    return attention_element - np.mean(attention_element)

def attention_nb_focal_speed_figure_data(model):
    print("Num neighbours: ", model.args['num_neighbours'])
    print("Future steps: ", model.args['future_steps'])
    X, Y = produce_mesh()

    Z_nb_speed = [produce_subnetwork_output(model, 'attention_model', X, Y,
                                            vn=speed, angle_vn=ANGLE)
                  for speed in cons.SPEED]
    offset_attention_in_place(Z_nb_speed)


    Z_focal_speed_ = [produce_subnetwork_output(model, 'attention_model', X, Y,
                                               vf=speed, angle_vn=ANGLE)
                      for speed in cons.SPEED]
    Z_focal_speed = list(map(offset_single_atention, Z_focal_speed_))

    return Z_nb_speed + Z_focal_speed, X, Y

def attention_single_figure_data(model, **kwargs):
    print("Num neighbours: ", model.args["num_neighbours"])
    print("Future steps: ", model.args["future_steps"])
    X, Y = produce_mesh()
    Z = produce_subnetwork_output(model, 'attention_model', X, Y, **kwargs)
    return offset_single_atention(Z), X, Y

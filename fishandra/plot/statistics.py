import numpy as np

from . import interaction
from . import product


class Statistics():
    def __init__(self, name, radius=8):
        self.radius = 8
        if name == 'interaction':
            self.prefix = ''
            self.produce_data = interaction.interactions_single_figure_data
        elif name == 'product':
            self.prefix = 'product_'
            self.produce_data = product.product_single_figure_data
        else:
            raise

    def add_prefix(self, out_dict):
        prefixed_dict = {self.prefix + key: out_dict[key] for key in out_dict}
        return prefixed_dict

    def __call__(self, Z, X, Y):
        return self.return_statistics(Z, X, Y)

    def from_model(self, model, delta_x=1, **kwargs):
        Z, X, Y = self.produce_data(model, **kwargs)
        return self.return_statistics(Z, X*delta_x, Y*delta_x)

    def return_statistics(self, Z, X, Y):
        stat_dict = {}

        mean_z_ = Z[0].flatten()
        x_ = X.flatten()
        y_ = Y.flatten()

        valid_points = x_*x_ + y_*y_ < self.radius*self.radius

        x = x_[valid_points]
        y = y_[valid_points]
        mean_z = mean_z_[valid_points]

        # The attraction-repulsion score is mean_z with a change of sign
        attraction_repulsion_score = mean_z
        attraction_repulsion_score[np.where(x < 0)] *= -1
        attraction_score = np.maximum(attraction_repulsion_score, 0)
        repulsion_score = -np.minimum(attraction_repulsion_score, 0)

        stat_dict['mean_attraction_repulsion'] = np.mean(
            attraction_repulsion_score)
        stat_dict['mean_attraction'] = np.mean(attraction_score)
        stat_dict['mean_repulsion'] = np.mean(repulsion_score)

        norm_r = np.sum(repulsion_score)

        def average_over_repulsion_score(f):
            return np.sum(f*repulsion_score)/norm_r

        stat_dict['repulsion_center_x'] = average_over_repulsion_score(x)
        stat_dict['repulsion_center_y'] = average_over_repulsion_score(y)
        stat_dict['repulsion_sd_x'] = np.sqrt(average_over_repulsion_score(
                                (x - stat_dict['repulsion_center_x'])**2))
        stat_dict['repulsion_sd_y'] = np.sqrt(average_over_repulsion_score(
                                (y - stat_dict['repulsion_center_y'])**2))

        return self.add_prefix(stat_dict)


product_statistics = Statistics('product')
interaction_statistics = Statistics('interaction')

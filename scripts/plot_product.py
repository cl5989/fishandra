#!/usr/bin/env python
import os
import matplotlib.pyplot as plt

from fishandra.script_utils import (load_model_and_update_constants,
                                    generate_arg_parser)

from fishandra.plot.new_constants import constants as cons
from fishandra.plot.product import product_nb_focal_speed_figure_data
from fishandra.plot.interaction import (plot_fig_1, plot_fig_2,
                                        plot_fig_3)

def product_figures(args):
    model = load_model_and_update_constants(args)

    models, X, Y = product_nb_focal_speed_figure_data(model, k=args.k)
    X, Y = X*cons.DELTA_X, Y*cons.DELTA_X

    plt.ion()
    # Fig1
    Zall = [ZandDZ[0] for ZandDZ in models]
    fig = plt.figure(1, (7, 5))
    limitsZ = plot_fig_1(fig, X, Y, Zall, limits=args.limitsZ)

    # Fig 2
    DZall = [ZandDZ[1] for ZandDZ in models]
    fig2 = plt.figure(2, (7, 6))
    limitsDZ = plot_fig_2(fig2, X, Y, DZall, limits=args.limitsZD)

    # Fig 3
    signal = [ZandDZ[2] for ZandDZ in models]
    fig3 = plt.figure(3, (7, 5))
    plot_fig_3(fig3, X, Y, Zall, DZall, signal, limits=[limitsZ, limitsDZ])

    # Save
    if not args.not_save_results:
        save_path = os.path.join(args.load_path, '_product_')
        for n in plt.get_fignums():
            plt.figure(n).savefig(save_path + str(n) + ".pdf", format='pdf')
    else:
        plt.ioff()
        plt.show()


if __name__ == '__main__':
    parser = generate_arg_parser()
    parser.add_argument('-k', type=float, default=None)
    parser.add_argument('--limitsZ', '-lz', type=int, default=None,
                        nargs=2, help='cbar attraction-repulsion')
    parser.add_argument('--limitsZD', '-lzd', type=int, default=None,
                        nargs=2, help='cbar alignment')
    args = parser.parse_args()
    product_figures(args)

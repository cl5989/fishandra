# fishandra

fishandra is a library to predict future turning using neural networks. It is named after fish (our main animal model) and Cassandra (Κασσάνδρα, mythological figure cursed with the ability to give future predictions that no one believed).

You can find some results using fishandra in the [PLOS Comp Biol article](https://doi.org/10.1371/journal.pcbi.1007354)

## Installation

### Requirements

fishandra requires some common packages such as scikit-learn, numpy and keras. Please refer to requirements.txt

fishandra also requires our package trajectorytools, that can be found in this [github repository](https://github.com/fjhheras/trajectorytools)

### Install

We recommend installing in develop mode with a symlink:

pip install -e .

## Quickstart

Use *fastrain* to train a model from trajectories

Use *fastest* to test a model with some trajectories

Look at the *scripts folder* to find some way to plot the trained models. Out of the box they only work for aggregation models as described in the PLOS Comp Biol article [1]. Some examples:

* *plot_mural.py* produces a figure like Figure 2
* *plot_interactions.py* produces figures like Figure 3
* *plot_attention.py* produces figures like Figure 5

## Documentation

Working on this

## Contributors

* Francisco J.H. Heras
* Francisco Romero-Ferrero

## Data

### Trajectories used in the PLOS Comp Biol article [1]

https://drive.google.com/drive/folders/1Oq7JPmeY3bXqPXc_oTUwUZbHU-m4uq_5?usp=sharing


## License

Copyright (C) 2018- Francisco J.H. Heras, Francisco Romero Ferrero
Robert Hinz, Gonzalo G. de Polavieja and the Champalimaud Foundation.

fishandra is free software (both as in freedom and as in free beer):
you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.
In addition, the authors chose to distribute it free of charge by making it
publicly available (https://gitlab.com/polavieja_lab/idtrackerai.git).

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. In addition, we require
derivatives or applications to acknowledge the authors by citing [1].

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

For more information please send an email (francisco.heras@neuro.fchampalimaud.org) or
use the tools available at https://gitlab.com/polavieja_lab/fishandra.git.

**[1] Heras, F.J.H., Romero-Ferrero, F., Hinz, R.C., de Polavieja, G.G.,
``Deep attention networks reveal the rules of collective motion in zebrafish''. PLoS Comp Biol 15.9 (2019): e1007354.**


